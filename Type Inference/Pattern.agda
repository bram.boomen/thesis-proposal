module Pattern where

module List-pattern where
  open import Agda.Builtin.List public

  pattern [_] z = z ∷ []
  pattern [_,_] y z = y ∷ z ∷ []
  pattern [_,_,_] x y z = x ∷ y ∷ z ∷ []
  pattern [_,_,_,_] w x y z = w ∷ x ∷ y ∷ z ∷ []
  pattern [_,_,_,_,_] v w x y z = v ∷ w ∷ x ∷ y ∷ z ∷ []
  pattern [_,_,_,_,_,_] u v w x y z = u ∷ v ∷ w ∷ x ∷ y ∷ z ∷ []
  pattern [_,_,_,_,_,_,_] t u v w x y z = t ∷ u ∷ v ∷ w ∷ x ∷ y ∷ z ∷ []
  pattern [_,_,_,_,_,_,_,_] s t u v w x y z = s ∷ t ∷ u ∷ v ∷ w ∷ x ∷ y ∷ z ∷ []
  pattern [_,_,_,_,_,_,_,_,_] r s t u v w x y z = r ∷ s ∷ t ∷ u ∷ v ∷ w ∷ x ∷ y ∷ z ∷ []
  pattern [_,_,_,_,_,_,_,_,_,_] q r s t u v w x y z = q ∷ r ∷ s ∷ t ∷ u ∷ v ∷ w ∷ x ∷ y ∷ z ∷ []

module Fin-pattern where
  open import FA-Inference.Base using (Fin; fs; f0)

  pattern 1'  = fs f0
  pattern 2'  = fs (fs f0)
  pattern 3'  = fs (fs (fs f0))
  pattern 4'  = fs (fs (fs (fs f0)))
  pattern 5'  = fs (fs (fs (fs (fs f0))))
  pattern 6'  = fs (fs (fs (fs (fs (fs f0)))))
  pattern 7'  = fs (fs (fs (fs (fs (fs (fs f0))))))
  pattern 8'  = fs (fs (fs (fs (fs (fs (fs (fs f0)))))))
  pattern 9'  = fs (fs (fs (fs (fs (fs (fs (fs (fs f0))))))))
  pattern 10' = fs (fs (fs (fs (fs (fs (fs (fs (fs (fs f0)))))))))
  pattern 11' = fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs f0))))))))))
  pattern 12' = fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs f0)))))))))))

module Type-pattern where
  open import FA-Inference.Base using (var; con)
  open import FA-Inference.Base using (S; NP; N)
  open import FA-Inference.Base using (Fin; fs; f0)

  pattern α = var (fs f0)
  pattern β = var (fs (fs f0))
  pattern γ = var (fs (fs (fs f0)))
  pattern δ = var (fs (fs (fs (fs f0))))
  pattern ε = var (fs (fs (fs (fs (fs f0)))))
  pattern ζ = var (fs (fs (fs (fs (fs (fs f0))))))
  pattern η = var (fs (fs (fs (fs (fs (fs (fs f0)))))))
  pattern θ = var (fs (fs (fs (fs (fs (fs (fs (fs f0))))))))
  pattern ι = var (fs (fs (fs (fs (fs (fs (fs (fs (fs f0)))))))))
  pattern κ = var (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs f0))))))))))
  pattern μ = var (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs f0)))))))))))
  pattern ν = var (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs f0))))))))))))
  pattern ξ = var (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs f0)))))))))))))
  pattern ο = var (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs f0))))))))))))))
  pattern π = var (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs f0)))))))))))))))
  pattern ρ = var (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs f0))))))))))))))))
  pattern σ = var (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs f0)))))))))))))))))
  pattern τ = var (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs f0))))))))))))))))))
  pattern υ = var (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs f0)))))))))))))))))))
  pattern ϕ = var (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs f0))))))))))))))))))))
  pattern χ = var (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs f0)))))))))))))))))))))
  pattern ψ = var (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs f0))))))))))))))))))))))
  pattern ω = var (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs (fs f0)))))))))))))))))))))))
  
  pattern 𝕤 = con S
  pattern 𝕟 = con N
  pattern 𝕟𝕡 = con NP
