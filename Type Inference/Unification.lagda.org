#+TITLE: Unification Module
#+DATE: \today
#+AUTHOR: Bram van den Boomen
#+LANGUAGE: en
#+LATEX_CLASS: agda
#+LATEX_HEADER: \newunicodechar{ℕ}{\ensuremath{\mathbb{N}}}
#+BIND: org-export-publishing-directory "./export"
#+PROPERTY: header-args :tangle source/Unification.agda
#+STARTUP: showall
#+OPTIONS: H:3 num:2

* Preface

#+begin_src agda2
module Unification where

open import Base

#+end_src

* Utils

#+begin_src agda2
_∘_ : {A B C : Set} → (B → C) → (A → B) → (A → C)
(f ∘ g) x = f (g x)

_▿ : {A B C : Set} → (A → B → C) → (B → A → C)
(f ▿) a b = f b a

▸_ : {S T : Set}(f : S → T) → (S → Maybe T)
▸ f = yes ∘ f

_◂ : {S T : Set}(f : S → Maybe T) → (Maybe S → Maybe T)
(f ◂) no = no
(f ◂) (yes s) = f s

▸_◂¹  : {S T : Set}(f : S → T) → (Maybe S → Maybe T)
▸ f ◂¹ = (▸ f) ◂

▸_◂²  : {R S T : Set}(f : R → S → T) → (Maybe R → Maybe S → Maybe T)
▸ f ◂² no no = no
▸ f ◂² no (yes t) = no
▸ f ◂² (yes r) no = no
▸ f ◂² (yes r) (yes s) = yes (f r s)

▹_ : {m n : Nat} → (Fin m → Fin n) → (Fin m → Type n)
▹ r = var ∘ r

_◃ : {m n : Nat} → (Fin m → Type n) → (Type m → Type n)
(f ◃) (con x) = con x
(f ◃) (var x) = f x
(f ◃) (arg ⟫ fun) = ((f ◃) arg) ⟫ ((f ◃) fun)
(f ◃) (fun ⟪ arg) = ((f ◃) fun) ⟪ ((f ◃) arg)

▹_◃ : {m n : Nat} → (Fin m → Fin n) → (Type m → Type n)
▹ f ◃ = (▹ f) ◃

_◇_ : {l m n : Nat}(f : Fin m → Type n) → (g : Fin l → Type m)
    → (Fin l → Type n)
f ◇ g = (f ◃) ∘ g
#+end_src

* Occur-check

#+begin_src agda2
thin : {n : Nat}(x : Fin (suc n))(y : Fin n) → Fin (suc n)
thin f0 y = fs y
thin (fs x) f0 = f0
thin (fs x) (fs y) = fs (thin x y)

thick : {n : Nat}(x y : Fin (suc n)) → Maybe (Fin n)
thick f0 f0 = no
thick f0 (fs y) = yes y
thick {suc n} (fs x) f0 = yes f0
thick {suc n} (fs x) (fs y) = ▸ fs ◂¹ (thick x y)

check : {n : Nat}(x : Fin (suc n))(t : Type (suc n)) → Maybe (Type n)
check x (con n) = yes (con n)
check x (var v) = ▸ var ◂¹ (thick x v)
check x (a ⟫ f) = ▸ _⟫_ ◂² (check x a) (check x f)
check x (f ⟪ a) = ▸ _⟪_ ◂² (check x f) (check x a)

_for_ : {n : Nat}(t' : Type n)(x : Fin (suc n)) → (Fin (suc n) → Type n)
(t' for x) y with thick x y
... | yes y' = var y'
... | no     = t'
#+end_src

* Substitution

#+begin_src agda2
data AList : (m n : Nat) → Set where
  []     : {m n : Nat} → AList n n
  _[_/_] : {m n : Nat}(σ : AList m n)(t' : Type m)(x : Fin (suc m))
            → AList (suc m) n

sub : {m n : Nat}(σ : AList m n) → (Fin m → Type n)
sub [] = var
sub (σ [ t' / x ]) = (sub σ) ◇ (t' for x)

_+A+_ : {l m n : Nat}(ρ : AList m n)(σ : AList l m) → AList l n
ρ +A+ [] = ρ
ρ +A+ (σ [ t / x ]) = (ρ +A+ σ) [ t / x ]

data ∃_ {S : Set}(T : S → Set) : Set where
  ⟨_,_⟩ : (s : S) → (t : T s) → ∃ T

_[_/_]' : {m : Nat}(a : ∃ (AList m))(t' : Type m)(x : Fin (suc m))
         → ∃ (AList (suc m))
⟨ n , σ ⟩ [ t' / x ]' = ⟨ n , σ [ t' / x ] ⟩

ø-sub : {m : Nat} → ∃ AList m
ø-sub {m} = ⟨ m , [] {m} ⟩

targ : {S : Set}{T : S → Set} → ∃ T → S
targ ⟨ n , t ⟩ = n
targ' : {m : Nat} → ∃ (AList m) → Nat
targ' ⟨ n , t ⟩ = n

∃⁻ : {S : Set}{T : S → Set} → (e : ∃ T) → T (targ e)
∃⁻ ⟨ s , t ⟩ = t

flexFlex : {m : Nat}(x y : Fin m) → ∃ (AList m)
flexFlex {zero} () y
flexFlex {suc m} x y with thick x y
flexFlex {suc m} x y | yes y' = ⟨ m , [] {m} [ var y' / x ] ⟩
flexFlex {suc m} x y | no = ⟨ (suc m) , [] {suc m} ⟩

flexRigid : {m : Nat}(x : Fin m)(t : Type m) → Maybe (∃ (AList m))
flexRigid {zero} () t
flexRigid {suc m} x t with check x t
... | yes t' = yes ⟨ m , [] {m} [ t' / x ] ⟩
... | no = no
#+end_src

* Most-general Unifier

#+begin_src agda2
amgu : {m : Nat}(s t : Type m)(acc : ∃ (AList m)) → Maybe (∃ (AList m))

amgu (con x) (a ⟫ f) acc = no
amgu (con x) (f ⟪ a) acc = no
amgu (a ⟫ f) (con x) acc = no
amgu (a ⟫ f) (g ⟪ b) acc = no
amgu (f ⟪ a) (b ⟫ g) acc = no
amgu (f ⟪ a) (con x) acc = no

amgu (con x) (con y) acc with x ≡N? y
... | yes p = yes acc
... | no ¬p = no
amgu (a ⟫ f) (b ⟫ g) acc = (amgu f g ◂) (amgu a b acc)
amgu (f ⟪ a) (g ⟪ b) acc = (amgu f g ◂) (amgu a b acc)

amgu (var v) (var w) ⟨ m , [] ⟩ = yes (flexFlex v w)
amgu (var x) t       ⟨ m , [] ⟩ = flexRigid x t
amgu t       (var x) ⟨ m , [] ⟩ = flexRigid x t
amgu t₁ t₂ ⟨ n , σ [ r / z ] ⟩  = let subs x = ((r for z) ◃) x
                                  in ▸ (λ σ → (σ [ r / z ]')) ◂¹
                                    (amgu (subs t₁) (subs t₂) ⟨ n , σ ⟩)

mgu : {m : Nat}(s t : Type m) → Maybe (∃ (AList m))
mgu {m} s t = amgu s t ⟨ m , [] {m} ⟩
#+end_src

* Application

#+begin_src agda2
apply¹ : {n : Nat} → List (Typed n) → ∃ AList n → List (∃ Typed)
apply¹ [] subs = []
apply¹ ((w := term) ∷ l) ⟨ s , t ⟩ = ⟨ s , w := ((sub t) ◃) term ⟩ ∷ apply¹ l ⟨ s , t ⟩

apply : {n : Nat} → List (Typed n) → List (∃ Typed)
apply {n} list = apply' list [] ⟨ n , [] {n} ⟩
  where
  apply' : {n : Nat} → List (Typed n) → List (Typed n) → ∃ AList n → List (∃ Typed)
  apply' [] acc subs = apply¹ acc subs
  apply' (x ∷ list) acc subs with apply'' x list subs
    where
    apply'' : {n : Nat} → Typed n → List (Typed n) → ∃ AList n → Maybe (∃ AList n)
    apply'' t [] subs = no
    apply'' (w₁ := t₁) ((w₂ := t₂) ∷ l) subs with w₁ =W? w₂
    apply'' (w₁ := t₁) ((w₂ := t₂) ∷ l) subs | no = apply'' (w₁ := t₁) l subs
    apply'' (w₁ := t₁) ((w₂ := t₂) ∷ l) subs | yes with amgu t₁ t₂ subs
    apply'' (w₁ := t₁) ((w₂ := t₂) ∷ l) subs | yes | yes subs' = yes subs'
    apply'' (w₁ := t₁) ((w₂ := t₂) ∷ l) subs | yes | no = apply'' (w₁ := t₁) l subs
  apply' (x ∷ list) acc subs | no = apply' list (x ∷ acc) subs
  apply' (x ∷ list) acc subs | yes subs' = apply' list acc subs'
#+end_src

#+begin_src agda2
apply'¹ : {n : Nat} → List (Typed n) → (sub : ∃ AList n) → List (Typed (targ sub))
apply'¹ [] subs = []
apply'¹ ((w := term) ∷ l) ⟨ s , t ⟩ = w := ((sub t) ◃) term ∷ apply'¹ l ⟨ s , t ⟩

apply-alt' : {n : Nat} → (Typed n) → List (Typed n) → Maybe (∃ AList n)
apply-alt' x [] = no
apply-alt' (w₁ := t₁) ((w₂ := t₂) ∷ l) with w₁ =W? w₂
apply-alt' (w₁ := t₁) ((w₂ := t₂) ∷ l) | no = apply-alt' (w₁ := t₁) l
apply-alt' (w₁ := t₁) ((w₂ := t₂) ∷ l) | yes with mgu t₁ t₂ 
apply-alt' (w₁ := t₁) ((w₂ := t₂) ∷ l) | yes | yes subs' = yes subs'
apply-alt' (w₁ := t₁) ((w₂ := t₂) ∷ l) | yes | no = apply-alt' (w₁ := t₁) l

{-# TERMINATING #-}
apply-alt : {n : Nat} → List (Typed n) → List (∃ Typed)
apply-alt l = apply-alt'' l []
  where
  apply-alt'' : {n : Nat} → List (Typed n) → List (Typed n) → List (∃ Typed)
  apply-alt'' {n} [] acc = map (λ ts → ⟨ n , ts ⟩) acc
  apply-alt'' (x ∷ list) acc with apply-alt' x list
  apply-alt'' (x ∷ list) acc | no = apply-alt'' list (x ∷ acc)
  apply-alt'' (x ∷ list) acc | yes subs = apply-alt'' (apply'¹ list subs) (apply'¹ acc subs)
#+end_src
