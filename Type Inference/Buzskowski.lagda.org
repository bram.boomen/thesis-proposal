#+TITLE: Buzskowski Module
#+DATE: \today
#+AUTHOR: Bram van den Boomen
#+LANGUAGE: en
#+LATEX_CLASS: agda
#+LATEX_HEADER: \newunicodechar{ℕ}{\ensuremath{\mathbb{N}}}
#+BIND: org-export-publishing-directory "./export"
#+PROPERTY: header-args :tangle source/Decorate.agda
#+STARTUP: showall
#+OPTIONS: H:3 num:2

* Preface

#+begin_src agda2
module Buzskowski where

open import Base
#+end_src

* Utils

#+begin_src agda2
#_ : (n : Nat) → Fin (suc n)
# zero = f0
# suc n = fs (# n)
##_ : {n : Nat} → Fin n → Nat
## f0 = zero
## fs f = suc (## f)

-- lifting a Fin or Type keeps the index of the Fin/Type, but raises the total number of elements.

_♯ : {m : Nat} → (Fin m) → Fin (suc m)
f0 ♯ = f0
(fs f) ♯ = fs (f ♯)
_♯' : {m : Nat} → (Type m) → Type (suc m)
(con x) ♯' = con x
(var x) ♯' = var (x ♯)
(a ⟫ f) ♯' = (a ♯') ⟫ (f ♯')
(f ⟪ a) ♯' = (f ♯') ⟪ (a ♯')

_♭_ : {m : Nat} → Fin m → Nat → Fin m
f0 ♭ zero = f0
f0 ♭ suc n = f0
fs f ♭ zero = fs f
fs f ♭ suc n = (f ♯) ♭ n
#+end_src

* Sorting

a < b & c = a < b
a < b & c > d

#+begin_src agda2
module Sort where

  open import Agda.Builtin.Equality
  open import Data.Nat using () renaming (_<_ to _<ℕ_; _<?_ to _<ℕ?_)
  open import Relation.Binary.PropositionalEquality using (cong)
  open import Data.Empty using (⊥)
  open import Data.String using () renaming (_<?_ to _<w?_; _<_ to _<w_)
  open import Relation.Nullary using (Dec; yes; no; ¬_)

  data _≤_ : Nat → Nat → Set where
    z≤s : ∀ {n : Nat} → zero ≤ n
    s≤s : ∀ {n m : Nat} → n ≤ m → (suc n) ≤ (suc m)
  
  ¬s≤z : ∀ {m : Nat} → ¬ (suc m ≤ zero)
  ¬s≤z ()
  
  ¬s≤s : ∀ {m n : Nat} → ¬ (m ≤ n) → ¬ (suc m ≤ suc n)
  ¬s≤s ¬m≤n (s≤s m≤n) = ¬m≤n m≤n
  
  _≤?_ : (n m : Nat) → Dec (n ≤ m)
  zero ≤? m = yes z≤s
  suc n ≤? zero = no ¬s≤z
  suc n ≤? suc m with n ≤? m
  ... | yes n≤m = yes (s≤s n≤m)
  ... | no ¬n≤m = no (¬s≤s ¬n≤m)

  ¬s=s : ∀ {m n : Nat} → ¬ (m ≡ n) → ¬ (suc m ≡ suc n)
  ¬s=s ¬m=n refl = ¬m=n refl

  _=?_ : (n m : Nat) → Dec (n ≡ m)
  zero =? zero = yes refl
  zero =? suc m = no (λ ())
  suc n =? zero = no (λ ())
  suc n =? suc m with n =? m
  (suc n =? suc .n) | yes refl = yes refl
  (suc n =? suc m) | no ¬p = no (¬s=s ¬p)
  
  ℕ? : {n : Nat} → Type n → Nat
  ℕ? (con x) = zero
  ℕ? (var x) = zero
  ℕ? (a ⟫ f) = suc (ℕ? a + ℕ? f)
  ℕ? (f ⟪ a) = suc (ℕ? f + ℕ? a)

  data _≼_ : {n m : Nat} → Type n → Type m → Set where
    c≼* : ∀ {x : Name}{n m : Nat}{t : Type n} → (con {m} x) ≼ t
    v≼* : ∀ {n m : Nat}{f : Fin m}{t : Type n} → (var f) ≼ t
    ⟫≺⟫ : ∀ {n m : Nat}{t t' : Type n}{s s' : Type m} → (ℕ? (t ⟫ t')) <ℕ (ℕ? (s ⟫ s')) → (t ⟫ t') ≼ (s ⟫ s')
    ⟪≺⟪ : ∀ {n m : Nat}{t t' : Type n}{s s' : Type m} → (ℕ? (t ⟪ t')) <ℕ (ℕ? (s ⟪ s')) → (t ⟪ t') ≼ (s ⟪ s')
    ⟫≺⟪ : ∀ {n m : Nat}{t t' : Type n}{s s' : Type m} → (ℕ? (t ⟫ t')) <ℕ (ℕ? (s ⟪ s')) → (t ⟫ t') ≼ (s ⟪ s')
    ⟪≺⟫ : ∀ {n m : Nat}{t t' : Type n}{s s' : Type m} → (ℕ? (t ⟪ t')) <ℕ (ℕ? (s ⟫ s')) → (t ⟪ t') ≼ (s ⟫ s')
    ⟫-+⟫ : ∀ {n m : Nat}{t t' : Type n}{s s' : Type m} → (ℕ? (t ⟪ t')) ≡ (ℕ? (s ⟪ s')) → t ≼ s → (t ⟫ t') ≼ (s ⟫ s')
    ⟪-+⟪ : ∀ {n m : Nat}{t t' : Type n}{s s' : Type m} → (ℕ? (t ⟪ t')) ≡ (ℕ? (s ⟪ s')) → t ≼ s → (t ⟪ t') ≼ (s ⟪ s')
    ⟫-+⟪ : ∀ {n m : Nat}{t t' : Type n}{s s' : Type m} → (ℕ? (t ⟪ t')) ≡ (ℕ? (s ⟪ s')) → t ≼ s → (t ⟫ t') ≼ (s ⟪ s')
    ⟪-+⟫ : ∀ {n m : Nat}{t t' : Type n}{s s' : Type m} → (ℕ? (t ⟪ t')) ≡ (ℕ? (s ⟪ s')) → t ≼ s → (t ⟪ t') ≼ (s ⟫ s')

  ¬⟫≼⟫ : ∀ {n m : Nat}{t t' : Type n}{s s' : Type m}
       → ¬ (ℕ? (t ⟫ t') <ℕ ℕ? (s ⟫ s')) → ¬ (ℕ? (t ⟫ t') ≡ ℕ? (s ⟫ s')) → ¬ ((t ⟫ t') ≼ (s ⟫ s'))
  ¬⟫≼⟫ ¬t<s ¬t=s (⟫≺⟫ x) = ¬t<s x
  ¬⟫≼⟫ ¬t<s ¬t=s (⟫-+⟫ x y) = ¬t=s x
  ¬⟪≼⟪ : ∀ {n m : Nat}{t t' : Type n}{s s' : Type m}
       → ¬ (ℕ? (t ⟪ t') <ℕ ℕ? (s ⟪ s')) → ¬ (ℕ? (t ⟪ t') ≡ ℕ? (s ⟪ s')) → ¬ ((t ⟪ t') ≼ (s ⟪ s'))
  ¬⟪≼⟪ ¬t<s ¬t=s (⟪≺⟪ x) = ¬t<s x
  ¬⟪≼⟪ ¬t<s ¬t=s (⟪-+⟪ x y) = ¬t=s x
  ¬⟫≼⟪ : ∀ {n m : Nat}{t t' : Type n}{s s' : Type m}
       → ¬ (ℕ? (t ⟫ t') <ℕ ℕ? (s ⟪ s')) → ¬ (ℕ? (t ⟫ t') ≡ ℕ? (s ⟪ s')) → ¬ ((t ⟫ t') ≼ (s ⟪ s'))
  ¬⟫≼⟪ ¬t<s ¬t=s (⟫≺⟪ x) = ¬t<s x
  ¬⟫≼⟪ ¬t<s ¬t=s (⟫-+⟪ x y) = ¬t=s x
  ¬⟪≼⟫ : ∀ {n m : Nat}{t t' : Type n}{s s' : Type m}
       → ¬ (ℕ? (t ⟪ t') <ℕ ℕ? (s ⟫ s')) → ¬ (ℕ? (t ⟪ t') ≡ ℕ? (s ⟫ s')) → ¬ ((t ⟪ t') ≼ (s ⟫ s'))
  ¬⟪≼⟫ ¬t<s ¬t=s (⟪≺⟫ x) = ¬t<s x
  ¬⟪≼⟫ ¬t<s ¬t=s (⟪-+⟫ x y) = ¬t=s x

  ¬⟫-+⟫ : ∀ {n m : Nat}{t t' : Type n}{s s' : Type m}
       → ¬ (ℕ? (t ⟫ t') <ℕ ℕ? (s ⟫ s')) → ¬ (t ≼ s) → ¬ ((t ⟫ t') ≼ (s ⟫ s'))
  ¬⟫-+⟫ ¬t<s ¬t≼s (⟫≺⟫ x) = ¬t<s x
  ¬⟫-+⟫ ¬t<s ¬t≼s (⟫-+⟫ x p) = ¬t≼s p
  ¬⟪-+⟪ : ∀ {n m : Nat}{t t' : Type n}{s s' : Type m}
       → ¬ (ℕ? (t ⟪ t') <ℕ ℕ? (s ⟪ s')) → ¬ (t ≼ s) → ¬ ((t ⟪ t') ≼ (s ⟪ s'))
  ¬⟪-+⟪ ¬t<s ¬t≼s (⟪≺⟪ x) = ¬t<s x
  ¬⟪-+⟪ ¬t<s ¬t≼s (⟪-+⟪ x p) = ¬t≼s p
  ¬⟫-+⟪ : ∀ {n m : Nat}{t t' : Type n}{s s' : Type m}
       → ¬ (ℕ? (t ⟫ t') <ℕ ℕ? (s ⟪ s')) → ¬ (t ≼ s) → ¬ ((t ⟫ t') ≼ (s ⟪ s'))
  ¬⟫-+⟪ ¬t<s ¬t≼s (⟫≺⟪ x) = ¬t<s x
  ¬⟫-+⟪ ¬t<s ¬t≼s (⟫-+⟪ x p) = ¬t≼s p
  ¬⟪-+⟫ : ∀ {n m : Nat}{t t' : Type n}{s s' : Type m}
       → ¬ (ℕ? (t ⟪ t') <ℕ ℕ? (s ⟫ s')) → ¬ (t ≼ s) → ¬ ((t ⟪ t') ≼ (s ⟫ s'))
  ¬⟪-+⟫ ¬t<s ¬t≼s (⟪≺⟫ x) = ¬t<s x
  ¬⟪-+⟫ ¬t<s ¬t≼s (⟪-+⟫ x p) = ¬t≼s p

  _≼?_ : {n m : Nat} → (t : Type n) → (t' : Type m) → Dec (t ≼ t')
  con x ≼? t = yes c≼*
  var x ≼? t = yes v≼*
  (t ⟫ t') ≼? con x = no (λ ())
  (t ⟫ t') ≼? var x = no (λ ())
  (t ⟪ t') ≼? con x = no (λ ())
  (t ⟪ t') ≼? var x = no (λ ())

  (t ⟫ t') ≼? (s ⟫ s') with ℕ? (t ⟫ t') <ℕ? ℕ? (s ⟫ s')
  (t ⟫ t') ≼? (s ⟫ s') | yes p = yes (⟫≺⟫ p)
  (t ⟫ t') ≼? (s ⟫ s') | no ¬p with ℕ? (t ⟫ t') =? ℕ? (s ⟫ s')
  (t ⟫ t') ≼? (s ⟫ s') | no ¬p | yes q with t ≼? s
  (t ⟫ t') ≼? (s ⟫ s') | no ¬p | yes q | yes r = yes (⟫-+⟫ q r)
  (t ⟫ t') ≼? (s ⟫ s') | no ¬p | yes q | no ¬r = no (¬⟫-+⟫ ¬p ¬r)
  (t ⟫ t') ≼? (s ⟫ s') | no ¬p | no ¬q = no (¬⟫≼⟫ ¬p ¬q)

  (t ⟪ t') ≼? (s ⟪ s') with ℕ? (t ⟪ t') <ℕ? ℕ? (s ⟪ s')
  (t ⟪ t') ≼? (s ⟪ s') | yes p = yes (⟪≺⟪ p)
  (t ⟪ t') ≼? (s ⟪ s') | no ¬p with ℕ? (t ⟪ t') =? ℕ? (s ⟪ s')
  (t ⟪ t') ≼? (s ⟪ s') | no ¬p | yes q with t ≼? s
  (t ⟪ t') ≼? (s ⟪ s') | no ¬p | yes q | yes r = yes (⟪-+⟪ q r)
  (t ⟪ t') ≼? (s ⟪ s') | no ¬p | yes q | no ¬r = no (¬⟪-+⟪ ¬p ¬r)
  (t ⟪ t') ≼? (s ⟪ s') | no ¬p | no ¬q = no (¬⟪≼⟪ ¬p ¬q)

  (t ⟪ t') ≼? (s ⟫ s') with ℕ? (t ⟪ t') <ℕ? ℕ? (s ⟫ s')
  (t ⟪ t') ≼? (s ⟫ s') | yes p = yes (⟪≺⟫ p)
  (t ⟪ t') ≼? (s ⟫ s') | no ¬p with ℕ? (t ⟪ t') =? ℕ? (s ⟫ s')
  (t ⟪ t') ≼? (s ⟫ s') | no ¬p | yes q with t ≼? s 
  (t ⟪ t') ≼? (s ⟫ s') | no ¬p | yes q | yes r = yes (⟪-+⟫ q r)
  (t ⟪ t') ≼? (s ⟫ s') | no ¬p | yes q | no ¬r = no (¬⟪-+⟫ ¬p ¬r)
  (t ⟪ t') ≼? (s ⟫ s') | no ¬p | no ¬q = no (¬⟪≼⟫ ¬p ¬q)

  (t ⟫ t') ≼? (s ⟪ s') with ℕ? (t ⟫ t') <ℕ? ℕ? (s ⟪ s')
  (t ⟫ t') ≼? (s ⟪ s') | yes p = yes (⟫≺⟪ p)
  (t ⟫ t') ≼? (s ⟪ s') | no ¬p with ℕ? (t ⟫ t') =? ℕ? (s ⟪ s')
  (t ⟫ t') ≼? (s ⟪ s') | no ¬p | yes q with t ≼? s
  (t ⟫ t') ≼? (s ⟪ s') | no ¬p | yes q | yes r = yes (⟫-+⟪ q r)
  (t ⟫ t') ≼? (s ⟪ s') | no ¬p | yes q | no ¬r = no (¬⟫-+⟪ ¬p ¬r)
  (t ⟫ t') ≼? (s ⟪ s') | no ¬p | no ¬q = no (¬⟫≼⟪ ¬p ¬q)
  
  data _<t_ : {n m : Nat} → Typed n → Typed m → Set where
    w<w : ∀ {v w : Word}{n m : Nat}{s : Type n}{t : Type m}
        → v <w w → (v := s) <t (w := t)
    t<t : ∀ {w : Word}{n m : Nat}{s : Type n}{t : Type m}
        → s ≼ t → (w := s) <t (w := t)
  
  postulate
    ¬v<v : ∀ {v : Word} → ¬ (v <w v)
    ⊥v<v : ∀ {v w : Word} → ¬ (v <w w) → v ≡ w → ⊥
  
  ¬w<w : {v w : Word}{n m : Nat}{s : Type n}{t : Type m} → ¬ (v <w w) → ¬ ((v := s) <t (w := t))
  ¬w<w         ¬v<w (w<w v<w) = ¬v<w v<w
  ¬w<w {v}{.v} ¬v<w (t<t s≺t) = ⊥v<v {v} {v} ¬v<w refl
  ¬t<t : {w : Word}{n m : Nat}{s : Type n}{t : Type m} → ¬ (s ≼ t) → ¬ ((w := s) <t (w := t))
  ¬t<t {w} ¬s≺t (w<w w<w') = ¬v<v {w} w<w'
  ¬t<t     ¬s≺t (t<t s≺t)  = ¬s≺t s≺t
  
  _<t?_ : {n m : Nat} → (t : Typed n) → (t' : Typed m) → Dec (t <t t')
  (w₁ := t₁) <t? (w₂ := t₂) with w₁ =W? w₂
  ((w₁ := t₁) <t? (w₂ := t₂)) | no with w₁ <w? w₂
  ((w₁ := t₁) <t? (w₂ := t₂)) | no | yes p = yes (w<w p)
  ((w₁ := t₁) <t? (w₂ := t₂)) | no | no ¬p = no (¬w<w ¬p)
  ((w₁ := t₁) <t? (.w₁ := t₂)) | yes with t₁ ≼? t₂
  ((w₁ := t₁) <t? (.w₁ := t₂)) | yes | yes p = yes (t<t p)
  ((w₁ := t₁) <t? (.w₁ := t₂)) | yes | no ¬p = no (¬t<t ¬p)
  
  insert : {A : Set}{_<_ : A → A → Set}(El : A) → List A → ((x : A)(y : A) → Dec (x < y)) → List A
  insert a [] _<?_ = a ∷ []
  insert a (x ∷ list) _<?_ with a <? x
  insert a (x ∷ list) _<?_ | no ¬p = x ∷ (insert a list _<?_)
  insert a (x ∷ list) _<?_ | yes p = a ∷ x ∷ list
  
  insertion-sort : {A : Set}{_<_ : A → A → Set} → ((x : A)(y : A) → Dec (x < y)) → List A → List A
  insertion-sort _<_ list = insertion-sort' _<_ list []
    where
    insertion-sort' : {A : Set}{_<_ : A → A → Set}→ ((x : A)(y : A) → Dec (x < y))
                   → List A → List A → List A
    insertion-sort' _<_ [] acc = acc
    insertion-sort' _<_ (x ∷ xs) [] = insertion-sort' _<_ xs (x ∷ [])
    insertion-sort' _<_ (x ∷ xs) (a ∷ acc) = insertion-sort' _<_ xs (insert x (a ∷ acc) _<_)

  TS-sort : {n : Nat} → List (Typed n) → List (Typed n)
  TS-sort = insertion-sort _<t?_

open Sort using (insert; insertion-sort; _≼?_; _<t?_; _≤?_) public
#+end_src

* Decorate

#+begin_src agda2
decorate : {m : Nat} → Struct → Fin m → Type m → List (Typed m) → List (Typed m)
decorate [ x ] fin t acc = insert (x := t) acc _<t?_
decorate {m} (a ▹ f) (fs (fs fin)) (var v) acc = let fin'  = fs fin ♯
                                                     fin'' = fin ♯ ♯
                                                 in decorate {m} f fin'' (var fin' ⟫ var v)
                                                    (decorate {m} a fin' (var fin') acc)
decorate {m} (f ◃ a) (fs (fs fin)) (var v) acc = let fin'  = fs fin ♯
                                                     fin'' = fin ♯ ♯
                                                 in decorate {m} f fin'' (var v ⟪ var fin')
                                                    (decorate {m} a fin' (var fin') acc)
decorate {m} (a ▹ f) (fs fin) t acc = let fin'  = fs fin
                                          fin'' = ((fin ♯) ♭ (no-args a))
                                      in decorate {m} f fin'' (var (fs fin) ⟫ t)
                                         (decorate {m} a fin' (var (fs fin)) acc)
decorate {m} (f ◃ a) (fs fin) t acc = let fin'  = fs fin
                                          fin'' = ((fin ♯) ♭ (no-args a))
                                      in decorate {m} f fin'' (t ⟪ var (fs fin))
                                         (decorate {m} a fin' (var (fs fin)) acc)
decorate s f0 t acc = acc 

decorateL : (s : List Struct) → Type (suc (no-largs s)) → List (Typed (suc (no-largs s)))
decorateL set target-type = let no-vars = (no-largs set)
                            in decorateL' {suc no-vars} set (# no-vars) target-type []
  where
  decorateL' : {m : Nat} → List Struct → Fin m → Type m → List (Typed m) → List (Typed m)
  decorateL' [] fin t acc = acc
  decorateL' (x ∷ ls) fin t acc = decorateL' ls (fin ♭ (no-args x)) t (decorate x fin  t acc)
#+end_src

* Put Constants

#+begin_src agda2
putcon : {m : Nat} → Typed m → List (Typed m) → List (Typed m)
putcon ts [] = []
putcon (w₁ := t₁) ((w₂ := t₂) ∷ l) with w₁ =W? w₂
putcon (w₁ := t₁) ((w₂ := t₂) ∷ l) | yes = (w₁ := t₁) ∷ putcon (w₁ := t₁) l
putcon (w₁ := t₁) ((w₂ := t₂) ∷ l) | no  = (w₂ := t₂) ∷ putcon (w₁ := t₁) l
#+end_src 
